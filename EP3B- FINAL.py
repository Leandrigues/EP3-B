"""
  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES
  DESSE EP E QUE PORTANTO NÃO CONSTITUEM DESONESTIDADE ACADÊMICA
  OU PLÁGIO.  
  DECLARO TAMBÉM QUE SOU RESPONSÁVEL POR TODAS AS CÓPIAS
  DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO E
  DESONESTIDADE ACADÊMICA SERÃO TRATADOS SEGUNDO OS CRITÉRIOS
  DIVULGADOS NA PÁGINA DA DISCIPLINA.
  ENTENDO QUE EPS SEM ASSINATURA NÃO SERÃO CORRIGIDOS E,
  AINDA ASSIM, PODERÃO SER PUNIDOS POR DESONESTIDADE ACADÊMICA.

  Nome : Leandro Rodrigues da Silva
  NUSP : 10723944
  Turma: 45 (BCC 2018)

  Referências: Documentação do pygame: https://www.pygame.org/docs/
"""
from random import randint
from glob import glob
import math
import sys
import pygame
import pygame.mixer
""" O MUNDO DE WUMPUS

    Esta é uma versão adaptada do jogo criado por Gregory Yob em 1975.
    Nesta versão o mundo é um tabuleiro em forma de toro NxN em que cada
    sala pode estar livre, conter um muro, um poço sem fundo ou um
    terrível Wumpus. As personagens nesse mundo devem se unir para 
    eliminar todos os Wumpus e assim poderem voltar sãs e salvas para
    casa. Ao percorrerem as salas do mundo, as personagens recebem dados
    sensoriais que as ajudarão a compreender o que pode estar nas salas
    adjacentes, e assim evitar alguns destinos trágico, como cair em um
    poço sem fundo ou ser devoradas por um terrível Wumpus. Cada
    personagem possui uma flecha, que pode ser disparada uma única vez,
    preferencialmente quando a personagem tiver acumulado evidências
    suficientes de que existe um Wumpus à sua frente. Personagens também
    podem se encontrar, e trocar conhecimento sobre as partes do mundo
    que já exploraram.
"""

# use apenas para depuração
__DEBUG__ = True
    
# o mundo não deve ser importado como móduloe ele me tirou duvida
if __name__!="__main__":
    print("Por favor execute o Mundo de Wumpus com o comando\n",
          "    python3 mundo.py")
    exit()

# associa números/nomes aos 4 tipos de salas do mundo de Wumpus
LIVRE,MURO,POCO,WUMPUS = range(4)
salas = ["L","M","P","W"]

# associa números/nomes aos 5 tipos de ações possíveis
ANDAR,GIRARDIREITA,GIRARESQUERDA,ATIRAR,COMPARTILHAR = range(5)
acoes = ["A","D","E","T","C"]


class MundoDeWumpus:
    """ Classe principal: define um Mundo de Wumpus, cria personagens,
        faz a simulação e anuncia o final do jogo.
    """
    def __init__(self):
        
        self.listaglob = glob('personagem*.py')
        self.P = len(self.listaglob)
        self.N = math.ceil(((0.9*self.P)/(1-0.2-0.2-0.5))**0.5)

        # Verificação da quantidade de argumentos de entrada
        if len(sys.argv) > 3:
            if len(sys.argv) == 4:
                self.nWumpus = math.floor(0.9*self.P)
                self.deltaLivres = 0.5*(self.N**2)
                self.betaPoços = math.floor(0.2*(self.N**2))
            if len(sys.argv) == 5:
                self.nWumpus = math.floor(0.9*self.P)
                self.deltaLivres = 0.5*(self.N**2)
                self.betaPoços = int(sys.argv[4])
            if len(sys.argv) == 6:
                self.nWumpus = int(sys.argv[5])
                self.deltaLivres = 0.5*(self.N**2)
                self.betaPoços = int(sys.argv[4])
            if len(sys.argv) == 7:
                self.nWumpus = int(sys.argv[5])
                self.betaPoços = int(sys.argv[4])
                self.deltaLivres = int(sys.argv[6])
            self.alfaMuros = int(sys.argv[3])
        else:
            self.alfaMuros = self.betaPoços = math.floor(0.2*(self.N**2))
            self.nWumpus = math.ceil(0.9*self.P)
            self.deltaLivres = 0.5*(self.N**2)

        self.mundo =[]
        for i in range(self.N) : 
            linha = []
            for j in range(self.N) : 
                linha.append([])
            self.mundo.append(linha)
        
        # Inicializa as salas livres
        for i in range(self.N):
            for j in range(self.N):
                self.mundo[i][j] = LIVRE

        # Sorteia os muros, wumpus e poços do mundo
        contadorAlfa = 0
        contadorBeta = 0
        contadorGama = 0
        while contadorAlfa < self.alfaMuros:
            salaSorteadaX = randint(0,self.N-1)
            salaSorteadaY = randint(0,self.N-1)
            while self.mundo[salaSorteadaX][salaSorteadaY] != LIVRE:
                salaSorteadaX = randint(0,self.N-1)
                salaSorteadaY = randint(0,self.N-1)
            self.mundo[salaSorteadaX][salaSorteadaY] = MURO
            contadorAlfa+=1
        while contadorBeta < self.betaPoços:
            salaSorteadaX = randint(0,self.N-1)
            salaSorteadaY = randint(0,self.N-1)
            while self.mundo[salaSorteadaX][salaSorteadaY] != LIVRE:
                salaSorteadaX = randint(0,self.N-1)
                salaSorteadaY = randint(0,self.N-1)
            self.mundo[salaSorteadaX][salaSorteadaY] = POCO
            contadorBeta+=1
        while contadorGama < self.nWumpus:
            salaSorteadaX = randint(0,self.N-1)
            salaSorteadaY = randint(0,self.N-1)
            while self.mundo[salaSorteadaX][salaSorteadaY] != LIVRE:
                salaSorteadaX = randint(0,self.N-1)
                salaSorteadaY = randint(0,self.N-1)
            self.mundo[salaSorteadaX][salaSorteadaY] = WUMPUS
            contadorGama+=1        
        
        N = math.ceil(((0.9*self.P)/(1-0.2-0.2-0.5))**0.5)
        self.listaPersonagens = []
        self.listaObj = ListaDePersonagens(self.mundo).lista
        for objeto in (self.listaObj):
            self.listaPersonagens.append(objeto)
        
        self.mundoCopia = self.mundo.copy() # Cria cópia do mundo
        # Chama a função que iniciará o processamento do jogo
        ListaDePersonagens(self.mundo).processaJogo(self.nWumpus, self.N, self.mundo, self.mundoCopia)
    
    # outras funções auxiliares do processamento do mundo
class Personagem:
    def __init__(self,N, modulo, mundo):
        # Pega a lista de arquivos, modulos e nomes das personagens
        lista = glob("personagem*.py")
        self.modulo = __import__(lista[0][:-3]) # importa tirando o .py do nome
        self.nome = lista[0][10:-3]

        self.salvaImpacto = None    # Variável para salvar a informação se o personagem bateu em um muro ou não

        # inicializa a personagem
        self.estaviva = True # bem-vinda ao Mundo de Wumpus, personagemNUSP!
        self.N = N 
        self.jaTocou = False

        # Inicializa o personagem em um lugar aleatório livre do mapa
        self.posicaoX = randint(0,N-1)
        self.posicaoY = randint(0,N-1)
        while mundo[self.posicaoX][self.posicaoY] != LIVRE:
            self.posicaoX = randint(0,N-1)
            self.posicaoY = randint(0,N-1)
        self.posicao = [self.posicaoX,self.posicaoY]
        self.orientacaoX = randint(-1,1)
        self.orientacaoY = randint(-1,1)
        while self.orientacaoX == self.orientacaoY or abs(self.orientacaoX) == abs(self.orientacaoY):
            self.orientacaoX = randint(-1,1)
            self.orientacaoY = randint(-1,1)  
        self.orientacao = [self.orientacaoX, self.orientacaoY]         

        self.nFlechas = 1
        self.modulo.inicializa(N) # chama a inicialização do módulo

        # define os valores que a personagemNUSP conhece
        self.modulo.nFlechas = self.nFlechas # copia nFlechas para o módulo
        self.modulo.mundoCompartilhado = [] # cria matriz NxN de listas vazias
        for i in range(N) :
            linha = []
            for j in range(N) :
                linha.append([]) # começa com listas vazias
            self.modulo.mundoCompartilhado.append(linha)


        # Usa um vetor com as funções acima para facilitar o processamento das ações.
        # Os índices correspondem aos valores atribuídos aos símbolos respectivos
        # ("A"<->0, "D"<->1, etc.)
        self.processe = [ self.ande, self.gireDireita, self.gireEsquerda, self.atire, self.compartilhe ]

    def planejar(self,percepcao):
        """ Método planejar (implementado pelo módulo)
        """
        self.modulo.planejar(percepcao)

    def verificaImpacto(self, mundo):
        pos = self.posicao
        ori = self.orientacao
        posnova = [(pos[0]+ori[0])%self.N,
                   (pos[1]+ori[1])%self.N]

        if mundo[posnova[0]][posnova[1]] == MURO:
            self.impacto = True
        else:
            self.impacto = False
        return self.impacto
    def agir(self):
        """ Método agir (implementado pelo módulo)
        """
        self.modulo.agir()

    def ande(self,MundoW):
        """ Função ande: verifica se é possível mover a personagem
            na direção indicada por sua orientação, e as consequências
            disso.
        """
        # posicao e orientacao atuais da personagem
        pos = self.posicao
        ori = self.orientacao
        # calcula a posição nova
        posnova = [(pos[0]+ori[0])%self.N,
                   (pos[1]+ori[1])%self.N]
        # se houver um muro, não dá para andar
        mundo = MundoW.mundo
        if mundo[posnova[0]][posnova[1]] == MURO:
            self.impacto = True
        else:
            pos[0],pos[1] = posnova[0],posnova[1]
            # se houver wumpus ou poço, é game over para a personagemNUSP
            if mundo[pos[0]][pos[1]] in [ WUMPUS, POCO ]:
                self.estaviva = False # NÃÃÃÃÃÃÃOOOOOOOOO!!!!!!!!!!!!
        # tentar andar é sempre realizável
        return True

    def gireDireita(self,MundoW):
        """ Corrige a orientação através de um giro no sentido horário.
        """
        ori = self.orientacao
        if ori[1]==0:
            ori[0] = -ori[0]
        ori[0],ori[1] = ori[1],ori[0]
        # girar é sempre realizável
        return True

    def gireEsquerda(self,MundoW):
        """ Corrige a orientação através de um giro no sentido anti-horário.
        """
        ori = self.orientacao
        if ori[0]==0:
            ori[1] = -ori[1]
        ori[0],ori[1] = ori[1],ori[0]
        # girar é sempre realizável
        return True

    def atire(self,MundoW):
        """ Atira uma flecha, se possível, na direção indicada pela
            orientação da personagemNUSP, e verifica se acertou um Wumpus.
        """
        # personagem só pode atirar se tiver flechas...
        if self.nFlechas==0:
            return False
        # processa o tiro
        self.nFlechas -= 1
        self.modulo.nFlechas = self.nFlechas
        # calcula destino do tiro
        pos = self.posicao
        ori = self.orientacao
        posnova = [(pos[0]+ori[0])%self.N,
                   (pos[1]+ori[1])%self.N]
        # verifica se acertou um Wumpus e atualiza o mundo
        mundo = MundoW.mundo
        if mundo[posnova[0]][posnova[1]] == WUMPUS:
            mundo[posnova[0]][posnova[1]] = LIVRE # atualiza a sala com Wumpus
            MundoDeWumpus().nWumpus -= 1 # contabiliza a morte
            MundoDeWumpus().urro = True # propaga o som da morte do Wumpus
        # informa que o tiro foi realizado
        return True

    def compartilhe(self,MundoW):
        """ Nesta 1a versão do jogo, o compartilhamento permite à personagemNUSP
            apenas enxergar o que a personagem dummy conhece do mundo, caso as
            duas estejam na mesma sala.
        """
        # testa se a personagemNUSP e dummy estão na mesma sala


        # transfere o conhecimento acumulado pela personagem dummy,
        # fazendo a conversão entre os sistemas de coordenadas 
        #for i in range(self.N):
        #    for j in range(self.N):
        #        # lembrando que a personagemNUSP começou na posição (2,2)
        #        # e olhando para a direita, mas pensava que estava na
        #        # posição (0,0) olhando para baixo...
        #        self.modulo.mundoCompartilhado[j-2][2-i] = MundoW.dummy.mundo[i][j].copy()
        # compartilhamento bem-sucedido!
        return True

class ListaDePersonagens: 
    def __init__(self,mundo):
        # Cria a lista de objetos da classe Personagem
        listaGlob= glob('personagem*.py')
        P = len(listaGlob)
        self.N = math.ceil(((0.9*P)/(1-0.2-0.2-0.5))**0.5)
        listaObjetos = []
        self.mundo = mundo
        for i in range(len(listaGlob)):
            modulo = Personagem(self.N,__import__(listaGlob[i][:-3]), self.mundo)
            listaObjetos.append(modulo)
        self.lista = listaObjetos

    def processaJogo(self, nWumpus, N,mundo, mundoCopia):
        deltaT = 1000
        fim = False
        #while nWumpus > 0 :
        while not fim:
            for personagem in (self.lista):
                contadorMortos = 0
                for i in range(len(self.lista)):
                    if self.lista[i].estaviva == False:
                        contadorMortos+=1   
                if personagem.estaviva and contadorMortos < len(self.lista):    # Verificação se todos personagens estão mortos
                    personagem.impacto = urro = False
                    personagem.percepcao = self.processaPercepcoes(personagem,N,mundo,urro) # Cria percepções da personagem
                    testeimpacto = personagem.verificaImpacto(mundo)
                    if testeimpacto:
                        personagem.salvaImpacto = True
                    else:
                        personagem.salvaImpacto = False
                    personagem.impacto = self.urro = False
                    self.processaPlanejamentos(personagem)
                    
                    # Processa os comandos de acelerar/desacelerar o jogo
                    pygame.init()
                    for event in pygame.event.get():
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_COMMA:
                                deltaT = math.ceil(deltaT*1.5)
                            if event.key == pygame.K_PERIOD:
                                deltaT = math.floor(deltaT/1.5) 
                    viavel = False
                    while not viavel:
                        # chama o método de ação da personagemNUSP
                        viavel = self.processaAcoes(personagem)
            
            interface(N).atualizaTela(N, mundo, mundoCopia, self.lista, deltaT)
    def processaAcoes(self,personagem):
        acao = personagem.modulo.agir()
        # processa a ação (passando o próprio objeto MundoDeWumpus como argumento)
        viavel = personagem.processe[acoes.index(acao)](self)
        return viavel
    def processaPlanejamentos(self,personagem):
        personagem.modulo.planejar(personagem.percepcao) 

    def processaPercepcoes(self, personagem,N, mundo, urro):
        # Faz o equivalente a função montaPercepção da classe MundoDeWumpus, porém,
        # agora mostra o nome de personagens na sala
        pos = personagem.posicao
        self.N = N
        self.mundo = mundo
        percepcao = []
        vizinhos = [ [(pos[0]+1)%self.N,pos[1]],
                     [(pos[0]-1)%self.N,pos[1]],
                     [pos[0],(pos[1]+1)%self.N],
                     [pos[0],(pos[1]-1)%self.N] ]
        for viz in vizinhos:
            if self.mundo[viz[0]][viz[1]] == WUMPUS and "F" not in percepcao:
                percepcao.append("F") # fedor
            if self.mundo[viz[0]][viz[1]] == POCO and "B" not in percepcao:
                percepcao.append("B") # brisa
        if personagem.impacto:
            percepcao.append("I")
        if urro:
            percepcao.append("U")

        # Verifica se há outro personagem na sala (excluindo a possibilidade de considerar ele mesmo)
        for personagemNaSala in self.lista:
            if pos == personagemNaSala.posicao and personagemNaSala.nome != personagem.nome:
                percepcao.append(personagemNaSala.nome)
        return percepcao

class interface:
    def __init__(self,N):
        # Inicialização dos atributos da interface
        if len(sys.argv) < 2:
            self.M = 800
        else:
            self.M = int(sys.argv[1])        
        self.screen = pygame.display.set_mode((self.M,self.M))
        self.screen.fill((179,179,179))
        self.tamanhoSala = self.M/N    
             
    def atualizaTela(self, N, mundo, mundoCopia, personagens, deltaT):
        pygame.init()   # Inicaliza o pygame
        pygame.mixer.init() # Inicializa o mixer do pygame
        pygame.display.set_caption("Mundo de Wumpus")

        # Inicialização dos arquivos de som do jogo
        wumpusComendo = pygame.mixer.Sound("wumpusComendo.wav")
        caindoPoço = pygame.mixer.Sound("caindoPoço.wav")
        impactoMuro = pygame.mixer.Sound("impactoMuro.wav")
        wumpusUrro = pygame.mixer.Sound("wumpusUrro.wav")

        # Inicialização e redimensionamento das imagens do jogo
        livreImagem = pygame.image.load("livre.png")
        livreImagem = pygame.transform.scale(livreImagem,(math.floor(self.tamanhoSala),math.floor(self.tamanhoSala)))
        muroImagem = pygame.image.load("muro.png")
        muroImagem = pygame.transform.scale(muroImagem,(math.floor(self.tamanhoSala),math.floor(self.tamanhoSala)))
        wumpusImagem = pygame.image.load("wumpus.png")
        wumpusImagem = pygame.transform.scale(wumpusImagem,(math.floor(self.tamanhoSala),math.floor(self.tamanhoSala))) 
        poçoImagem = pygame.image.load("poço.png")
        poçoImagem = pygame.transform.scale(poçoImagem,(math.floor(self.tamanhoSala),math.floor(self.tamanhoSala))) 
        baixoImagem = pygame.image.load("baixo.png")
        baixoImagem = pygame.transform.scale(baixoImagem,(math.floor(self.tamanhoSala),math.floor(self.tamanhoSala)))
        cimaImagem = pygame.image.load("cima.png")
        cimaImagem = pygame.transform.scale(cimaImagem,(math.floor(self.tamanhoSala),math.floor(self.tamanhoSala)))
        direitaImagem = pygame.image.load("direita.png")
        direitaImagem = pygame.transform.scale(direitaImagem,(math.floor(self.tamanhoSala),math.floor(self.tamanhoSala)))
        esquerdaImagem = pygame.image.load("esquerda.png")
        esquerdaImagem = pygame.transform.scale(esquerdaImagem,(math.floor(self.tamanhoSala),math.floor(self.tamanhoSala))) 


        pygame.time.delay(deltaT)
                
                #print('a')
                #deltaT = deltaT*1.5


        # Percorre o mundo para colocar as imagens de cada sala
        for i in range(N):
            for j in range(N):
                if mundo[i][j] == WUMPUS:
                    self.screen.blit(wumpusImagem, [(self.tamanhoSala*j), (self.tamanhoSala*i)])
                if mundo[i][j] == LIVRE:
                    self.screen.blit(livreImagem, [(self.tamanhoSala*j), (self.tamanhoSala*i)])
                if mundo[i][j] == MURO:
                    self.screen.blit(muroImagem, [(self.tamanhoSala*j), (self.tamanhoSala*i)])
                if mundo[i][j] == POCO:
                    self.screen.blit(poçoImagem, [(self.tamanhoSala*j), (self.tamanhoSala*i)])    
                if mundo[i][j] == LIVRE and mundoCopia[i][j] == WUMPUS:
                    wumpusUrro.play()
        for personagem in personagens:
            posX = personagem.posicao[0]
            posY = personagem.posicao[1]

            # Execução dos sons das personagens
            if mundo[posX][posY] == WUMPUS and personagem.jaTocou == False:
                personagem.jaTocou = True
                wumpusComendo.play()
            if mundo[posX][posY] == POCO and personagem.jaTocou == False:
                personagem.jaTocou = True
                caindoPoço.play()
            if personagem.salvaImpacto:
                impactoMuro.play()

            # Posicionamento das imagens das personagens
            if mundo[posX][posY] != WUMPUS and mundo[posX][posY] != POCO:
                if personagem.orientacao == [1,0]:
                    self.screen.blit(baixoImagem, [((self.tamanhoSala*posY)),((self.tamanhoSala*posX))])
                if personagem.orientacao == [-1,0]:
                    self.screen.blit(cimaImagem, [((self.tamanhoSala*posY)),((self.tamanhoSala*posX))])
                if personagem.orientacao == [0,1]:
                    self.screen.blit(direitaImagem,[((self.tamanhoSala*posY)),((self.tamanhoSala*posX))])
                if personagem.orientacao == [0,-1]:
                    self.screen.blit(esquerdaImagem,[((self.tamanhoSala*posY)),((self.tamanhoSala*posX))])
            



        
        pygame.display.update()











m = MundoDeWumpus()

    
